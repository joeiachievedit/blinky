//
// blinky
//

uint8_t yellowLed;
uint8_t greenLed;

void setup() {
  yellowLed = 12;
  greenLed  = 11;
  
  pinMode(yellowLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
}

void loop() {
  digitalWrite(yellowLed, HIGH);
  digitalWrite(greenLed, LOW);
  
  delay(250);
  
  digitalWrite(yellowLed, LOW);
  digitalWrite(greenLed, HIGH);
  
  delay(250);
}
